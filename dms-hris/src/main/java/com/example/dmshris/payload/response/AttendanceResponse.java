package com.example.dmshris.payload.response;

import com.example.dmshris.model.Attendance;
import com.example.dmshris.util.EventType;
import com.example.dmshris.util.Workplace;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
public class AttendanceResponse implements Serializable {
    @JsonProperty(value = "attendance_id")
    private UUID attendanceId;
    @JsonProperty(value = "clock_in")
    private LocalDateTime clockIn;
    @JsonProperty(value = "clock_out")
    private LocalDateTime clockOut;
    @JsonProperty(value = "attendance_date")
    private LocalDate attendanceDate;

    @JsonProperty(value = "work_place")
    private Workplace workplace;
    private EventType event;
    private ProjectResponse project;
    @JsonProperty(value = "daily_task")
    private String dailyTask;

    @JsonProperty(value = "total_work")
    private Long totalWork;
    private EmployeeResponse employee;
    public static AttendanceResponse mapResponse(Attendance attendanceLog) {
        AttendanceResponse attendanceLogResponse = new AttendanceResponse();
        attendanceLogResponse.setAttendanceId(attendanceLog.getAttendanceId());
        attendanceLogResponse.setClockIn(attendanceLog.getClockIn());
        attendanceLogResponse.setClockOut(attendanceLog.getClockOut());
        attendanceLogResponse.setWorkplace(Workplace.valueOf(attendanceLog.getWorkplace()));
        attendanceLogResponse.setEvent(EventType.valueOf(attendanceLog.getEvent()));
        attendanceLogResponse.setProject(
                Objects.isNull(attendanceLog.getProject())?null: new ProjectResponse(
                        attendanceLog.getProject().getProjectId(), attendanceLog.getProject().getProjectName(),
                        Objects.isNull(
                                attendanceLog.getProject().getManager()) ? null :
                                attendanceLog.getProject().getManager().getFullName()));
        attendanceLogResponse.setDailyTask(attendanceLog.getDailyTask());
        attendanceLogResponse.setTotalWork(attendanceLog.getTotalWork());
        attendanceLogResponse.setEmployee(
                Objects.isNull(
                        attendanceLog.getEmployee())? null :
                        EmployeeResponse.mapResponse(attendanceLog.getEmployee()));
        attendanceLogResponse.setAttendanceDate(attendanceLog.getAttendanceDate());
        return attendanceLogResponse;
    }
}
