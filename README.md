# <center> API DOC HRIS </center>
# 1. Attendances
## a. GET /attendances

_Response (200 - OK)_

```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "attendanceId": "UUID",
            "clockIn": "LocalDateTime",
            "clockOut": "LocalDateTime",
            "attendanceDate": "LocalDate",
            "createdDate": "LocalDate",
            "createdBy": "LocalDate",
            "workplace": "String",
            "event": "String",
            "project": 
                {
                    "projectId": "Long",
                    "projectName": "String",
                    "manager": "String"
                }
            ,
            "dailyTask": "String",
            "totalWork": "Long",
            "employee": 
                {
                    "employeeId": "Long",
                    "fullName": "String",
                    "email": "String",
                    "phoneNumber": "String",
                    "joinDate": "Date",
                    "manager": "String",
                    "departmentName": "String",
                    "client": 
                        {
                            "clientId": "Long",
                            "clientName": "String",
                            "email": "String",
                            "notes": "String"
                        }
                    ,
                    "project": 
                        {
                            "projectId": "Long",
                            "projectName": "String",
                            "manager": "String"
                        }
                }
        }
    ]
}
```
## b. POST /attendances/clock-in
_Request_

- body:
```json
{
    "attendanceId": "String",
    "clockIn": "LocalDateTime",
    "clockOut": "LocalDateTime",
    "workplace": "String",
    "dailyTask": "String",
    "projectId": "Long",
    "attendanceDate": "LocalDate"
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "attendanceId": "UUID",
            "clockIn": "LocalDateTime",
            "clockOut": "LocalDateTime",
            "attendanceDate": "LocalDate",
            "createdDate": "LocalDate",
            "createdBy": "LocalDate",
            "workplace": "String",
            "event": "String",
            "project": 
                {
                    "projectId": "Long",
                    "projectName": "String",
                    "manager": "String"
                }
            ,
            "dailyTask": "String",
            "totalWork": "Long",
            "employee": 
                {
                    "employeeId": "Long",
                    "fullName": "String",
                    "email": "String",
                    "phoneNumber": "String",
                    "joinDate": "Date",
                    "manager": "String",
                    "departmentName": "String",
                    "client": 
                        {
                                "clientId": "Long",
                                "clientName": "String",
                                "email": "String",
                                "notes": "String"
                        }
                    ,
                    "project": 
                        {
                                "projectId": "Long",
                                "projectName": "String",
                                "manager": "String"
                        }
                }
        }
    ]
}
```

## c. POST /attendances/clock-out
_Request_

- body:
```json
{
    "attendanceId": "String",
    "clockIn": "LocalDateTime",
    "clockOut": "LocalDateTime",
    "workplace": "String",
    "dailyTask": "String",
    "projectId": "Long",
    "attendanceDate": "LocalDate"
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "attendanceId": "UUID",
            "clockIn": "LocalDateTime",
            "clockOut": "LocalDateTime",
            "attendanceDate": "LocalDate",
            "createdDate": "LocalDate",
            "createdBy": "LocalDate",
            "workplace": "String",
            "event": "String",
            "project": 
                {
                    "projectId": "Long",
                    "projectName": "String",
                    "manager": "String"
                }
            ,
            "dailyTask": "String",
            "totalWork": "Long",
            "employee": 
                {
                    "employeeId": "Long",
                    "fullName": "String",
                    "email": "String",
                    "phoneNumber": "String",
                    "joinDate": "Date",
                    "manager": "String",
                    "departmentName": "String",
                    "client": 
                        {
                            "clientId": "Long",
                            "clientName": "String",
                            "email": "String",
                            "notes": "String"
                        }
                    ,
                    "project": 
                        {
                            "projectId": "Long",
                            "projectName": "String",
                            "manager": "String"
                        }
                }
        }
    ]
}
```
&nbsp;
# 2. Attendances Log
## a. POST /attendance-log/for-period
_Request_

- body:
```json
{
    "period": {
        "start": "LocalDate",
        "end": "LocalDate"
    },
    "paging": {
        "pageNo": "Integer (default = 0)",
        "pageSize": "Integer (default = 10)",
        "orderBy": "String",
        "descending": "Boolean (default = true)"
    }

}
```

_Response (200 - OK)_
```json
{
    "errorCode": "null",
    "errorMessage": "null",
    "data": [
        {
            "attendanceId": "UUID",
            "clockIn": "LocalDateTime",
            "clockOut": "LocalDateTime",
            "workplace": "String",
            "event": "String",
            "project": 
                {
                    "projectId": "Long",
                    "projectName": "String",
                    "fullName": "String"
                },
            "dailyTask": "String",
            "totalWork": "Long",
            "employee": [{                 
                            "employeeId": "Long",
                            "fullName": "String",
                            "email": "String",
                            "phoneNumber": "String",
                            "joinDate": "Date",
                            "isManager": "Boolean",
                            "isActive": "Boolean",
                            "manager": "Long",
                            "jobId": "Long",
                            "departmentId": "Long",
                            "clientId": "Long"
                        }],
            "attendanceDate": "LocalDate"
        }
    ]
}
```

## b. POST /attendance-log/for-period-with-status
_Request_

- body:
```json
{
    "period": {
        "start": "LocalDate",
        "end": "LocalDate"
    },
    "paging": {
        "pageNo": "Integer (default = 0)",
        "pageSize": "Integer (default = 10)",
        "orderBy": "String",
        "descending": "Boolean (default = true)"
    },
    "statusList": ["ANNUAL_LEAVE", "SICK_LEAVE", "MATERNITY_LEAVE", "OTHER", "PUBLIC_HOLIDAY", "ATTEND"]
}
```

_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": []
}
```

## c. POST /attendance-log/summary-for-period
_Request_

- body:
```json
{
    "period": {
        "start": "LocalDate",
        "end": "LocalDate"
    }
}
```

_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "onTime": "Long",
            "late": "Long",
            "absent": "Long",
            "onTimePercentage": "Double",
            "latePercentage": "Double",
            "absentPercentage": "Double"
        }
    ]
}
```
&nbsp;

# 3. Client
## a. POST /clients
_Request_

- body:
```json
{
   "clientName": "String",
   "email": "String",
   "notes": "String",
   "location": {
                    "streetAddress": "String",
                    "postalCode": "String",
                    "province": "String",
                    "countryId": "Long"
               },
}
```

_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "clientId": "Long",
            "clientName": "String",
            "email": "String",
            "notes": "String"
        }
    ]
}
```

## b. GET /clients
_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "clientId": "Long",
            "clientName": "String",
            "email": "String",
            "notes": "String"
        }
    ]
}
```

&nbsp;

# 4. Country
## a. GET /countries
_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "countryId": "Long",
            "countryName": "String",
            "alpha2Code": "String",
            "alpha3Code": "String",
            "numericCode": "String"
        }
    ]
}
```
&nbsp;

# 5. Department
## a. POST /departments
_Request_

- body:
```json
{
   "departmentName": "String",
   "managerId": "Long",
}
```

_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "errorMessage": "String"
}
```
## b. GET /departments
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "departmentId": "Long",
            "departmentName": "String",
            "manager": "String"
        }
    ]
}
```
&nbsp;

# 6. Employee
## a. POST /employees
_Request_

- body:
```json
{
    "fullName": "String",
    "email": "String",
    "phoneNumber": "String",
    "joinDate": "Date",
    "jobId": "Long",
    "departmentId": "Long",
    "projectIdList": ["Long"],
    "clientId": "Long"
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": ["String"]
}
```
## b. GET /employees
_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "employeeId": "Long",
            "fullName": "String",
            "email": "String",
            "phoneNumber": "String",
            "joinDate": "Date",
            "isManager": "Boolean",
            "isActive": "Boolean",
            "manager": "Long",
            "jobId": "Long",
            "departmentId": "Long",
            "clientId": "Long"
        }
    ]
}
```
## c. DELETE /employees/:employee-id
_Params_
```json
{
    "employee-id": "Long"
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": ["String"]
}
```
## d. GET /employees/managers
_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "employeeId": "Long",
            "fullName": "String",
            "email": "String",
            "phoneNumber": "String",
            "joinDate": "Date",
            "isManager": "Boolean",
            "isActive": "Boolean",
            "manager": "Long",
            "jobId": "Long",
            "departmentId": "Long",
            "clientId": "Long"
        }
    ]
}
```
## e. POST /employees/assign-job
_Request_
- body:
```json
{
    "employeeId": "Long",
    "jobId": "Long"   
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "employeeId": "Long",
            "fullName": "String",
            "email": "String",
            "phoneNumber": "String",
            "joinDate": "Date",
            "isManager": "Boolean",
            "isActive": "Boolean",
            "manager": "Long",
            "jobId": "Long",
            "departmentId": "Long",
            "clientId": "Long"
        }
    ]
}
```
## f. POST /employees/assign-department
_Request_
- body:
```json
{
    "employeeId": "Long",
    "jobId": "Long"   
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "employeeId": "Long",
            "fullName": "String",
            "email": "String",
            "phoneNumber": "String",
            "joinDate": "Date",
            "isManager": "Boolean",
            "isActive": "Boolean",
            "manager": "Long",
            "jobId": "Long",
            "departmentId": "Long",
            "clientId": "Long"
        }
    ]
}
```
## g. POST /employees/assign-project
_Request_
- body:
```json
{
    "employeeId": "Long",
    "projectIdList": "[Long]"   
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "employeeId": "Long",
            "fullName": "String",
            "email": "String",
            "phoneNumber": "String",
            "joinDate": "Date",
            "isManager": "Boolean",
            "isActive": "Boolean",
            "manager": "Long",
            "jobId": "Long",
            "departmentId": "Long",
            "clientId": "Long"
        }
    ]
}
```
&nbsp;

# 7. Job
## a. POST /jobs
_Request_
- body:
```json
{
    "jobTitle": "String",
    "jobDescription": "String"
}
```
_Response (201 - CREATED)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": ["String"]
}
```
## b. GET /jobs
_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "jobId": "Long",
            "jobTitle": "String",
            "jobDescription": "String"
        }
    ]
}
```
&nbsp;

# 8. Overtime
## a. GET /overtime/all
_Response (200 - OK)_
```json
[
    {
        "full_name": "String",
        "periode": "String",
        "total_duration": "String",
        "date": "String"
    }
]
```
&nbsp;

# 9. Project
## a. POST /projects
_Request_
- body:
```json
{
    "projectName": "String",
    "managerId": "Long",
    "clientId": "Long"
}
```
_Response (201 - Created)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": {"String"},
    "HttpStatus": FORBIDDEN
}
```
## b. GET /projects
_Response (200 - OK)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": [
        {
            "projectId": "Long",
            "projectName": "String",
            "manager": "String"
        }
    ]
}
```
&nbsp;

# 10. Timesheet
## a. GET /timesheets
_Params_
```json
{
    "month": "Long"
}
```
_Headers_
```json
"Content-Disposition: attachment; filename={EmployeeName(String)}_timesheet_{month(String)}.xlsx"
```
_Body_
```json
"Excel file"
```
_Response (406 - NOT_ACCEPTABLE)_
```json
{
    "errorCode": "String",
    "errorMessage": "String",
    "data": ["String"],
    "statusCode": "String"
}
```