package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
public class AssignProjectRequest implements Serializable {
    @JsonProperty(value = "employee_id")
    private Long employeeId;

    @JsonProperty(value = "project_id_list")
    private Set<Long> projectIdList;
}
