package com.example.dmshris.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public record ProjectResponse (
        @JsonProperty(value = "project_id")
        Long projectId,

        @JsonProperty(value = "project_name")
        String projectName,

        @JsonProperty(value = "manager")
        String manager
) implements Serializable {
}
