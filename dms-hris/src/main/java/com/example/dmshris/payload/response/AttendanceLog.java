package com.example.dmshris.payload.response;

import com.example.dmshris.model.Employee;
import com.example.dmshris.model.Project;
import com.example.dmshris.util.EventType;
import com.example.dmshris.util.Workplace;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceLog {
    @JsonProperty(value = "attendance_id")
    private UUID attendanceId;

    @JsonProperty(value = "clockIn")
    private LocalDateTime clockIn;

    @JsonProperty(value = "clockOut")
    private LocalDateTime clockOut;

    @JsonProperty(value = "workplace")
    private Workplace workplace;

    @JsonProperty(value = "event")
    private EventType event;

    private Project project;

    private String dailyTask;

    private Long totalWork;

    private Employee employee;

    @JsonProperty(value = "attendanceDate")
    private LocalDate attendanceDate;

    private LocalDateTime createdDate;
}
