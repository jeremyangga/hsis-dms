package com.example.dmshris.payload.request;

import com.example.dmshris.util.EventType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class AttendanceLogForPeriodWithStatusRequest implements Serializable {
    PeriodRequest period = new PeriodRequest();
    PagingRequest paging = new PagingRequest();
    @JsonProperty(value = "status_list")
    List<EventType> statusList = new ArrayList<>();
}
