package com.example.dmshris.service;

import com.example.dmshris.exception.CommonException;
import com.example.dmshris.model.Job;
import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.JobRequest;
import com.example.dmshris.payload.response.JobResponse;
import com.example.dmshris.repository.JobRepository;
import com.example.dmshris.util.CommonMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class JobService {
    private final JobRepository repository;

    public JobService(JobRepository repository) {
        this.repository = repository;
    }

    public CommonResponse addDataJob(JobRequest request) {
        if(repository.existsByJobTitle(request.getJobTitle())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Job title %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getJobTitle()),
                    HttpStatus.FORBIDDEN
            );
        }
        Job job = new Job();
        job.setJobTitle(request.getJobTitle());
        job.setJobDescription(request.getJobDescription());
        repository.save(job);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse listAllJobs(){
        List<JobResponse> jobs = new ArrayList<>();
        repository.findAll().forEach(data -> jobs.add(new JobResponse(
                data.getJobId(), data.getJobTitle(), data.getJobDescription()
        )));
        if(!jobs.isEmpty()) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, jobs);
        } else {
            return new CommonResponse(CommonMessage.NO_DATA_CODE, CommonMessage.NO_DATA_MESSAGE, CommonMessage.NO_DATA_MESSAGE);
        }
    }

    public Job getJobById(Long jobId) {
        return repository.findById(jobId).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for job your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE
        ));
    }
}
