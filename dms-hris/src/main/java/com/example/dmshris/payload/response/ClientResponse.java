package com.example.dmshris.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record ClientResponse (
    @JsonProperty(value = "client_id")
    Long clientId,
    @JsonProperty(value = "client_name")
    String clientName,
    String email,
    String notes) implements Serializable {}
