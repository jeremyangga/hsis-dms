package com.example.dmshris.model;

import jakarta.persistence.Column;

import java.io.Serializable;

public class JobHistoryKey implements Serializable {
    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "job_id")
    private Long jobId;

    @Column(name = "department_id")
    private Long departmentId;
}
