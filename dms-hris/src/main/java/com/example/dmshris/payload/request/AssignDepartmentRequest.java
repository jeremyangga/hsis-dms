package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AssignDepartmentRequest implements Serializable {
    @JsonProperty(value = "employee_id")
    private Long employeeId;

    @JsonProperty(value = "department_id")
    private Long departmentId;
}
