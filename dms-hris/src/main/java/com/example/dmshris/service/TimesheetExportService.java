package com.example.dmshris.service;

import com.example.dmshris.dto.TimesheetDTO;
import com.example.dmshris.util.TimesheetClientCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.sql.Time;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
public class TimesheetExportService {
    private final AttendanceService attendanceService;
    private final EmployeeService employeeService;
    private final ProjectService projectService;

    public TimesheetExportService(AttendanceService attendanceService, EmployeeService employeeService, ProjectService projectService) {
        this.attendanceService = attendanceService;
        this.employeeService = employeeService;
        this.projectService = projectService;
    }

    private void fillData(Sheet sheet, List<TimesheetDTO> timesheetEntries) {
        if(timesheetEntries.get(0).getClientCode() == null || timesheetEntries.get(0).getClientCode() == TimesheetClientCode.OTHER) {
            sheet.getRow(5).getCell(3).setCellValue(STR.": \{timesheetEntries.get(0).getEmployeeName()}");
            sheet.getRow(6).getCell(3).setCellValue(STR.": \{timesheetEntries.get(0).getDate()}");
            sheet.getRow(7).getCell(3).setCellValue(STR.": \{timesheetEntries.get(timesheetEntries.size() - 1).getDate()}");
        }
        int rowNum = 11;
        for (TimesheetDTO entry : timesheetEntries) {
            Row row = sheet.getRow(rowNum++);
            row.getCell(0).setCellValue(rowNum);
            row.getCell(2).setCellValue(entry.getDate().format(DateTimeFormatter.ofPattern("E")));
            row.getCell(3).setCellValue(String.valueOf(entry.getDate()));
            row.getCell(4).setCellValue(entry.getTask());
            if(!entry.getProjectName().isEmpty()){
                row.getCell(4).setCellValue("In Project");
                row.getCell(5).setCellValue(entry.getProjectName());
            }
            row.getCell(6).setCellValue(entry.getTask());
            row.getCell(7).setCellValue(entry.getDetail());

            row.getCell(9).setCellValue(entry.getClockIn().format(DateTimeFormatter.ofPattern("HH:mm")));
            row.getCell(10).setCellValue(entry.getClockOut().format(DateTimeFormatter.ofPattern("HH:mm")));
            row.getCell(11).setCellValue(entry.getHoursWorked());
        }
    }
    private InputStream getTemplateInputStream(TimesheetClientCode clientCode) throws IOException {
        String templatePath = STR."templates\\timesheet\\\{clientCode.toString()}.xlsx";
        return new ClassPathResource(templatePath).getInputStream();
    }
    public ByteArrayInputStream exportTimesheet(List<TimesheetDTO> timesheetEntries) {
        if (timesheetEntries == null || timesheetEntries.isEmpty()) {
            throw new IllegalArgumentException("Timesheet entries must not be null or empty");
        }
        try(
                InputStream templateInputStream = getTemplateInputStream(timesheetEntries.get(0).getClientCode());
                XSSFWorkbook workbook= new XSSFWorkbook(templateInputStream)
        ) {
            if (templateInputStream == null) {
                throw new FileNotFoundException("Template InputStream is null");
            }
                Sheet sheet = workbook.getSheet("Timesheet");
                fillData(sheet, timesheetEntries);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                workbook.write(outputStream);
                return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
