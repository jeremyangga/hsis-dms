package com.example.dmshris.controller;

import com.example.dmshris.dto.OvertimeGetAllDTO;
import com.example.dmshris.repository.OvertimeRepository;
import jakarta.persistence.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/overtime")
public class OvertimeController {
    private final OvertimeRepository overtimeRepository;

    @Autowired
    public OvertimeController(OvertimeRepository overtimeRepository) {
        this.overtimeRepository = overtimeRepository;
    }

    @GetMapping("/all")
    public List<OvertimeGetAllDTO> getOvertimeDetails() {
        List<Tuple> tuples = overtimeRepository.getAllOvertimeDetails();
        return  overtimeRepository.mapToDTO(tuples);
    }
}
