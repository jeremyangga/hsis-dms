package com.example.dmshris.repository;

import com.example.dmshris.model.Project;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Hidden
public interface ProjectRepository extends JpaRepository<Project, Long> {
    boolean existsByProjectName(String projectName);
    Optional<Project> findTopByStatusOrderByProjectIdDesc(Boolean isActive);
}
