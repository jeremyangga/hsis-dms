package com.example.dmshris.payload.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceLogSummaryResponse implements Serializable {
    private Long onTime;
    private Long late;
    private Long absent;
    private Double onTimePercentage;
    private Double latePercentage;
    private Double absentPercentage;
}
