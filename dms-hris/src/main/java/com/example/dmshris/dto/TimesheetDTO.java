package com.example.dmshris.dto;

import com.example.dmshris.util.TimesheetClientCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimesheetDTO {
    private LocalDate date;
    private LocalDate startDate;
    private LocalDate endDate;
    private String task;
    private String detail;
    private LocalDateTime clockIn;
    private LocalDateTime clockOut;
    private String employeeName;
    private String projectName;
    private double hoursWorked;
    private TimesheetClientCode clientCode;
}
