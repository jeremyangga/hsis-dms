package com.example.dmshris.repository;

import com.example.dmshris.model.Job;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Hidden
public interface JobRepository extends JpaRepository<Job, Long> {
    boolean existsByJobTitle(String jobTitle);
}
