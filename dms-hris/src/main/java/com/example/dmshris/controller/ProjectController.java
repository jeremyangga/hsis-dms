package com.example.dmshris.controller;

import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AssignManagerProject;
import com.example.dmshris.payload.request.ProjectRequest;
import com.example.dmshris.service.ProjectService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("projects")
@Slf4j
public class ProjectController {
    private final ProjectService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public ProjectController(ProjectService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<CommonResponse> addDataProject(@Valid @RequestBody ProjectRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataProject on class %s with payload %s", ProjectController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.addDataProject(request);
        log.info(String.format("Leaving method addDataProject on class %s", ProjectController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<CommonResponse> getAllDataProjects() throws JsonProcessingException {
        log.info(String.format("Entering method getAllDataProjects on class %s", ProjectController.class.getName()));
        CommonResponse response = service.listAllProjects();
        log.info(String.format("Leaving method getAllDataProjects on class %s with response %s", ProjectController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

//    @PostMapping("/assign-manager")
//    public ResponseEntity<CommonResponse> assignManagerToProject(@Valid @RequestBody AssignManagerProject request) throws JsonProcessingException {
//        log.info(String.format("Entering method assignManagerToProject on class %s with payload %s", ProjectController.class.getName(), mapper.writeValueAsString(request)));
//        CommonResponse response = service.assignManager(request);
//        log.info(String.format("Leaving method assignManagerToProject on class %s", ProjectController.class.getName()));
//        return new ResponseEntity<>(response, HttpStatus.CREATED);
//    }
}
