package com.example.dmshris.exception;

import com.example.dmshris.payload.CommonResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExceptionInterceptor extends ResponseEntityExceptionHandler {
    @ExceptionHandler(CommonException.class)
    public ResponseEntity<?> handleApplicationException(final CommonException exception, final HttpServletRequest request) {
        var response = new CommonResponse(
                exception.getErrorCode(),
                exception.getErrorMessage(),
                exception.getData()
        );
        return new ResponseEntity<>(response, exception.getStatusCode());
    }
}
