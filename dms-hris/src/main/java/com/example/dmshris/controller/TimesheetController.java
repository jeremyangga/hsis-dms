package com.example.dmshris.controller;

import com.example.dmshris.dto.TimesheetDTO;
import com.example.dmshris.exception.CommonException;
import com.example.dmshris.model.Attendance;
import com.example.dmshris.model.Employee;
import com.example.dmshris.repository.AttendanceRepository;
import com.example.dmshris.service.EmployeeService;
import com.example.dmshris.service.TimesheetExportService;
import com.example.dmshris.util.CommonMessage;
import com.example.dmshris.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("timesheets")
@Slf4j
public class TimesheetController {
    private final TimesheetExportService timesheetExportService;
    private final AttendanceRepository attendanceRepository;
    private final EmployeeService employeeService;


    public TimesheetController(TimesheetExportService timesheetExportService, AttendanceRepository attendanceRepository, EmployeeService employeeService) {
        this.timesheetExportService = timesheetExportService;
        this.attendanceRepository = attendanceRepository;
        this.employeeService = employeeService;
    }

    private List<TimesheetDTO> createTimesheetData() {
        Employee employee = employeeService.getEmployeeByEmail(CommonUtil.currentUser());
        List<Attendance> attendances = attendanceRepository.findByEmployeeAndTotalWorkIsNotNull(employee);
        if(attendances.isEmpty()) {
            return new ArrayList<>(null);
        }
        return attendances.stream().map(el -> {
            return new TimesheetDTO(
                    el.getAttendanceDate(), null, null,
                    el.getDailyTask(), null,
                    el.getClockIn(),
                    el.getClockOut(),
                    el.getEmployee().getFullName(),
                    el.getProject().getProjectName(),
                    el.getTotalWork(),
                    el.getEmployee().getClient().getTimesheetClientCode()
            );
        }).collect(Collectors.toList());
    }
    @GetMapping
    public ResponseEntity<InputStreamResource> exportTimesheet(@PathVariable @RequestParam("month") int month) {
        List<TimesheetDTO> timesheetEntries = createTimesheetData();
        if(timesheetEntries.isEmpty()) {
            throw new CommonException(
                    CommonMessage.ERROR_CODE,
                    CommonMessage.ERROR_MESSAGE,
                    CommonMessage.NOT_FOUND_MESSAGE,
                    HttpStatus.NOT_ACCEPTABLE
            );
        }
        ByteArrayInputStream exportedTimesheet = timesheetExportService.exportTimesheet(timesheetEntries);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=" + timesheetEntries.get(0).getEmployeeName() + "_timesheet_" + Month.of(month) + ".xlsx");
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_OCTET_STREAM).body(new InputStreamResource(exportedTimesheet));
    }
}
