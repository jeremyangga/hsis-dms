package com.example.dmshris.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OvertimeGetAllDTO {
    public String full_name;
    public String periode;
    public String total_duration;
    public String date;
}
