package com.example.dmshris.payload.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public record JobResponse(
        @JsonProperty(value = "job_id")
        Long jobId,

        @JsonProperty(value = "job_title")
        String jobTitle,

        @JsonProperty(value = "job_description")
        String jobDescription
) implements Serializable {
}
