package com.example.dmshris.repository;

import com.example.dmshris.dto.OvertimeGetAllDTO;
import com.example.dmshris.model.Overtime;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.persistence.Tuple;
import org.hibernate.sql.ast.tree.expression.Over;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public interface OvertimeRepository extends JpaRepository<Overtime, UUID> {
    @Query(value = "SELECT * FROM overtime_all_view", nativeQuery = true)
    List<Tuple> getAllOvertimeDetails();

    default List<OvertimeGetAllDTO> mapToDTO(List<Tuple> tuples) {
        return tuples.stream()
                .map(tuple -> new OvertimeGetAllDTO(
                        tuple.get("full_name", String.class),
                        tuple.get("periode", String.class),
                        tuple.get("total_duration", String.class),
                        tuple.get("date", String.class)
                ))
                .collect(Collectors.toList());
    }
}
