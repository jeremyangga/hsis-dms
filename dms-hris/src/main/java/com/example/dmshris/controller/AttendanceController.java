package com.example.dmshris.controller;

import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AttendanceRequest;
import com.example.dmshris.service.AttendanceService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/attendances")
@Slf4j
public class AttendanceController {
    private final AttendanceService attendanceService;
    public AttendanceController(AttendanceService attendanceService) {
        this.attendanceService = attendanceService;
    }

    @PostMapping("/clock-in")
    public ResponseEntity<CommonResponse> clockIn(@Valid @RequestBody AttendanceRequest attendanceRequest){
        CommonResponse commonResponse = attendanceService.clockIn(attendanceRequest);
        return new ResponseEntity<>(commonResponse, HttpStatus.CREATED);
    }

    @PostMapping("/clock-out")
    public ResponseEntity<CommonResponse> clockOut(@Valid @RequestBody AttendanceRequest attendanceRequest){
        CommonResponse commonResponse = attendanceService.clockOut(attendanceRequest);
        return new ResponseEntity<>(commonResponse, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<CommonResponse> getAttendanceRecord(){
        CommonResponse commonResponse = attendanceService.getAttendanceRecord();
        return new ResponseEntity<>(commonResponse, HttpStatus.OK);
    }
}
