package com.example.dmshris.repository;

import com.example.dmshris.model.Employee;
import com.example.dmshris.model.EmployeeProject;
import com.example.dmshris.model.ProjectKey;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Hidden
public interface EmployeeProjectRepository extends JpaRepository<EmployeeProject, ProjectKey> {
    List<EmployeeProject> findByEmployeeAndIsActive(Employee employee, boolean isActive);
}
