package com.example.dmshris.controller;

import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.JobRequest;
import com.example.dmshris.service.JobService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("jobs")
@Slf4j
public class JobController {
    private final JobService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public JobController(JobService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<CommonResponse> addDataJob(@Valid @RequestBody JobRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataJob on class %s with payload %s", JobController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.addDataJob(request);
        log.info(String.format("Leaving method addDataJob on class %s", JobController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<CommonResponse> getAllDataJobs() throws JsonProcessingException {
        log.info(String.format("Leaving method getAllDataJobs on class %s", JobController.class.getName()));
        CommonResponse response = service.listAllJobs();
        log.info(String.format("Leaving method getAllDataJobs on class %s with response %s", JobController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
