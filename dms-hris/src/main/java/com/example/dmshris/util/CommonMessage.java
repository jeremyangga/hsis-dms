package com.example.dmshris.util;

public class CommonMessage {
    private CommonMessage() {}

    public static final String SUCCESS_CODE = "200000";
    public static final String SUCCESS_MESSAGE = "success";

    public static final String NOT_FOUND_CODE = "200520";
    public static final String NOT_FOUND_MESSAGE = "data not found";

    public static final String ALREADY_EXIST_CODE = "200401";
    public static final String ALREADY_EXIST_MESSAGE = "already exist";

    public static final String NOT_ALLOWED_CODE = "200403";
    public static final String NOT_ALLOWED_MESSAGE = "is not allowed";

    public static final String NO_DATA_CODE = "200444";
    public static final String NO_DATA_MESSAGE = "no data found";

    public static final String ERROR_CODE = "200002";
    public static final String ERROR_MESSAGE = "Transaction failed";

    public static final String ALREADY_CLOCK_IN = "User has already clocked in for today.";
    public static final String ALREADY_CLOCK_OUT = "User has already clocked out for today.";

    public static final String NOT_YET_CLOCK_IN_CODE = "200123";
    public static final String NOT_YET_CLOCK_IN_MESSAGE = "User has no clocked in yet for today.";

    public static final String INVALID_AUTH_CODE = "200401";
    public static final String INVALID_AUTH_MESSAGE = "invalid authorization";

    public static final String INVALID_CODE = "200422";
    public static final String INVALID_MESSAGE = "invalid";

    public static final String NULL_CODE = "200400";
    public static final String NULL_MESSAGE = "invalid request content";
}
