package com.example.dmshris.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

public class OvertimeDTO {
    @JsonProperty("overtime_date")
    private LocalDate date;
    @JsonProperty ("clock_in")
    private LocalDateTime clockIn;
    @JsonProperty ("clock_out")
    private LocalDateTime clockOut;
    @JsonProperty ("user_id")
    private UUID UserID;
    @JsonProperty ("reason")
    private String reason;
    @JsonProperty ("project_id")
    private Long projectId;
}
