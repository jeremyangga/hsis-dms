package com.example.dmshris.controller;

import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.service.CountryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("countries")
@Slf4j
public class CountryController {
    private final CountryService service;
    private final ObjectMapper mapper = new ObjectMapper();

    public CountryController(CountryService service){
        this.service = service;
    }
    @GetMapping
    public ResponseEntity<CommonResponse> getAllCountries() throws JsonProcessingException {
        log.info(String.format("Entering method getAllCountries on class %s", CountryController.class.getName()));
        CommonResponse response = service.listAllCountries();
        log.info(String.format("Entering method getAllCountries on class %s with response %s", CountryController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
