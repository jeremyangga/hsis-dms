package com.example.dmshris.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "overtime")
@Data
public class Overtime {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "overtime_id", nullable = false)
    private UUID overtimeId;

    @Column(name = "clock_in")
    private LocalDateTime clockIn;

    @Column(name = "reason")
    private String reason;

    @Column(name = "clock_out")
    private LocalDateTime clockOut;

    @Column(name = "over_time_date")
    private LocalDateTime overTimeDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "employee_id", nullable = false)
    @JsonIgnore
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "project_id", referencedColumnName = "project_id", nullable = true, insertable = false, updatable = false)
    @JsonIgnore
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assessor_id", nullable = true)
    private Employee assessor;
}
