package com.example.dmshris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
// for bypass login (development) use this ==> (exclude = {SecurityAutoConfiguration.class}) delete it if done development
public class DmsHrisApplication {

	public static void main(String[] args) {
		SpringApplication.run(DmsHrisApplication.class, args);
	}

}
