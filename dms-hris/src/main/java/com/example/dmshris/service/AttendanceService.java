package com.example.dmshris.service;

import com.example.dmshris.exception.CommonException;
import com.example.dmshris.model.Attendance;
import com.example.dmshris.model.Employee;
import com.example.dmshris.model.Project;
import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AttendanceRequest;
import com.example.dmshris.repository.AttendanceRepository;
import com.example.dmshris.util.CommonMessage;
import com.example.dmshris.util.CommonUtil;
import com.example.dmshris.util.EventType;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@Getter
@Setter
//@Entity
public class AttendanceService {
    private final AttendanceRepository attendanceRepository;
    private final EmployeeService employeeService;
    private final ProjectService projectService;

    public AttendanceService(AttendanceRepository attendanceRepository, EmployeeService employeeService, ProjectService projectService) {
        this.attendanceRepository = attendanceRepository;
        this.employeeService = employeeService;
        this.projectService = projectService;
    }

    public CommonResponse clockIn(AttendanceRequest request) {
        System.out.println(CommonUtil.currentUser());
        Employee employee = employeeService.getEmployeeByEmail(CommonUtil.currentUser());
        Project project = projectService.getProjectById(request.getProjectId());
        Optional<Attendance> clockIn = attendanceRepository.findByAttendanceDateAndEmployeeAndProjectAndClockInIsNotNullOrderByClockInDesc(LocalDate.now(), employee, project);

        Attendance attendance = new Attendance();

        clockIn.ifPresentOrElse(data -> {
          throw new CommonException(CommonMessage.ERROR_CODE, CommonMessage.ERROR_MESSAGE, CommonMessage.ALREADY_CLOCK_IN, HttpStatus.FORBIDDEN);
        }, () ->{
            attendance.setClockIn(Objects.isNull(request.getClockIn()) ? LocalDateTime.now() : request.getClockIn());
            attendance.setWorkplace(String.valueOf(CommonUtil.setWorkplaceValue(request.getWorkplace())));
            attendance.setDailyTask(request.getDailyTask());
            attendance.setEmployee(employee);
            attendance.setProject(project);
            attendance.setEvent(String.valueOf(EventType.ATTEND));
            attendance.setTotalWork(0L);
            attendance.setAttendanceDate(Objects.isNull(request.getAttendanceDate()) ? LocalDate.now(): request.getAttendanceDate());
        });
        Attendance result = attendanceRepository.save(attendance);

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, result);
    }

    public CommonResponse clockOut(AttendanceRequest request) {
        Optional<Attendance> clockIn = attendanceRepository.findById(UUID.fromString(request.getAttendanceId()));
        Attendance attendance = new Attendance();
        clockIn.ifPresentOrElse(data -> {
            attendance.setAttendanceDate(data.getAttendanceDate());
            attendance.setClockIn(data.getClockIn());
            attendance.setClockOut(Objects.isNull(request.getClockOut()) ? LocalDateTime.now() : request.getClockOut());
            attendance.setAttendanceId(data.getAttendanceId());
            attendance.setCreatedBy(data.getCreatedBy());
            attendance.setCreatedDate(data.getCreatedDate());
            attendance.setDailyTask(Objects.isNull(request.getDailyTask())?data.getDailyTask():request.getDailyTask());
            attendance.setEmployee(data.getEmployee());
            attendance.setEvent(data.getEvent());
            attendance.setProject(data.getProject());
            attendance.setTotalWork(ChronoUnit.HOURS.between(data.getClockIn(), data.getClockOut()));
//            attendance.setUpdatedBy(CommonUtil.currentUser()); -> Cannot resolve method 'setUpdatedBy' in 'Attendance'
//            attendance.setUpdatedDate(LocalDateTime.now()); Cannot resolve method 'setUpdatedBy' in 'Attendance'
            attendance.setWorkplace(data.getWorkplace());
        }, () -> {
            throw new CommonException(
                    CommonMessage.NOT_YET_CLOCK_IN_CODE,
                    CommonMessage.ERROR_MESSAGE,
                    CommonMessage.NOT_YET_CLOCK_IN_MESSAGE,
                    HttpStatus.NOT_ACCEPTABLE
            );
        });
        Attendance result  = attendanceRepository.save(attendance);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, result);
    }

    public CommonResponse getAttendanceRecord() {
        List<Attendance> attendances = attendanceRepository.findByCreatedBy(CommonUtil.currentUser());
        if (attendances.isEmpty()) {
            return new CommonResponse(CommonMessage.NOT_FOUND_CODE, CommonMessage.NOT_FOUND_MESSAGE, CommonMessage.NOT_FOUND_MESSAGE);
        }
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, attendances);
    }
}
