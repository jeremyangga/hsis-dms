package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AssignManager implements Serializable {
    @JsonProperty(value = "department_id")
    private Long departmentId;

    @JsonProperty(value = "manager_id")
    private Long managerId;
}
