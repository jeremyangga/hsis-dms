package com.example.dmshris.controller;

import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AssignManager;
import com.example.dmshris.payload.request.DepartmentRequest;
import com.example.dmshris.service.DepartmentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("departments")
@Slf4j
public class DepartmentController {
    private final DepartmentService service;
    private final ObjectMapper mapper = new ObjectMapper();
    public DepartmentController(DepartmentService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<CommonResponse> addDataDepartment(@Valid @RequestBody DepartmentRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method addDataDepartment on class %s with payload %s", DepartmentController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.addDataDepartment(request);
        log.info(String.format("Leaving method addDataDepartment on class %s", DepartmentController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<CommonResponse> getAllDataDepartment() throws JsonProcessingException {
        log.info(String.format("Entering method getAllDepartments on class %s", DepartmentController.class.getName()));
        CommonResponse response = service.listAllDepartments();
        log.info(String.format("Entering method getAllDepartments on class %s with response %s", DepartmentController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

//    @PostMapping("/assign-manager")
//    public ResponseEntity<CommonResponse> assignManagerToDepartment(@Valid @RequestBody AssignManager request) throws JsonProcessingException {
//        log.info(String.format("Entering method assignManagerToDepartment on class %s with payload %s", DepartmentController.class.getName(), mapper.writeValueAsString(request)));
//        CommonResponse response = service.assign(request);
//        log.info(String.format("Leaving method assignManagerToDepartment on class %s", DepartmentController.class.getName()));
//        return new ResponseEntity<>(response, HttpStatus.CREATED);
//    }
}
