package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationRequest {
    @JsonProperty(value = "street_address")
    private String streetAddress;

    @JsonProperty(value = "postal_code")
    private String postalCode;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "province")
    private String province;

    @JsonProperty(value = "country_id")
    private Long countryId;
}
