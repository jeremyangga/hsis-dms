package com.example.dmshris.service;

import com.example.dmshris.exception.CommonException;
import com.example.dmshris.model.*;
import com.example.dmshris.model.auth.User;
import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AssignDepartmentRequest;
import com.example.dmshris.payload.request.AssignJobRequest;
import com.example.dmshris.payload.request.AssignProjectRequest;
import com.example.dmshris.payload.request.EmployeeRequest;
import com.example.dmshris.payload.response.EmployeeResponse;
import com.example.dmshris.repository.EmployeeProjectRepository;
import com.example.dmshris.repository.EmployeeRepository;
import com.example.dmshris.repository.UserRepository;
import com.example.dmshris.util.CommonMessage;
import com.example.dmshris.util.CommonUtil;
import com.example.dmshris.util.RoleType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Service
public class EmployeeService {
    private final EmployeeRepository repository;
    private final JobService jobService;
    private final DepartmentService departmentService;
    private final ProjectService projectService;
    private final ClientService clientService;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final EmployeeProjectRepository employeeProjectRepository;
    private final RestTemplate restTemplate;

    @Autowired
    public EmployeeService(EmployeeRepository repository, JobService jobService, DepartmentService departmentService, ProjectService projectService, ClientService clientService, PasswordEncoder passwordEncoder, UserRepository userRepository, EmployeeProjectRepository employeeProjectRepository, RestTemplate restTemplate) {
        this.repository = repository;
        this.jobService = jobService;
        this.departmentService = departmentService;
        this.projectService = projectService;
        this.clientService = clientService;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.employeeProjectRepository = employeeProjectRepository;
        this.restTemplate = restTemplate;
    }

    public CommonResponse addDataEmployee(EmployeeRequest request) throws URISyntaxException {
        if (repository.existsByEmail(request.getEmail())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Email %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getEmail()),
                    HttpStatus.FORBIDDEN);
        }

        if (!request.getEmail().contains("appfuxion.id") && !request.getEmail().contains("appfuxion.com")) {
            throw new CommonException(
                    CommonMessage.NOT_ALLOWED_CODE,
                    CommonMessage.NOT_ALLOWED_MESSAGE,
                    String.format("Email %s ".concat(CommonMessage.NOT_ALLOWED_MESSAGE), request.getEmail()),
                    HttpStatus.BAD_REQUEST);
        }

        if (!Objects.isNull(request.getPhoneNumber()) && repository.existsByPhoneNumber(request.getPhoneNumber())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Phone Number %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getPhoneNumber()),
                    HttpStatus.FORBIDDEN);
        }

        if (Objects.isNull(request.getJobId())) {
            throw new CommonException(
                    CommonMessage.NULL_CODE,
                    CommonMessage.NULL_MESSAGE,
                    String.format("%s for data job", CommonMessage.INVALID_MESSAGE),
                    HttpStatus.FORBIDDEN
            );
        }

        if (Objects.isNull(request.getProjectIdList())) {
            Project project = projectService.getFirstProject();
            request.setProjectIdList(Collections.singleton(project.getProjectId()));

            if (Objects.isNull(request.getClientId())) {
                request.setClientId(project.getClient().getClientId());
            }
        }

        Employee employee = new Employee();
        employee.setFullName(request.getFullName());
        employee.setEmail(request.getEmail());
        employee.setPhoneNumber(request.getPhoneNumber());
        employee.setJoinDate(request.getJoinDate());
        employee.setIsManager(false);
        employee.setIsActive(true);
        employee.setClient(clientService.getClientById(request.getClientId()));
        employee.setJob(jobService.getJobById(request.getJobId()));
        employee.setDepartment(Objects.isNull(request.getDepartmentId()) ? null : departmentService.getDepartmentById(request.getDepartmentId()));
        employee.setClient(Objects.isNull(request.getClientId()) ? null : clientService.getClientById(request.getClientId()));
        repository.saveAndFlush(employee);

        request.getProjectIdList().forEach(projectId -> {
            EmployeeProject employeeProject = new EmployeeProject();
            ProjectKey id = new ProjectKey();
            id.setEmployeeId(employee.getEmployeeId());
            id.setProjectId(projectId);
            employeeProject.setEmployeeProjectId(id);
            employeeProject.setEmployee(employee);
            employeeProject.setProject(projectService.getProjectById(projectId));
            employeeProject.setStartDate(request.getJoinDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            employeeProject.setEndDate(employeeProject.getStartDate().plusMonths(6));
            employeeProject.setIsActive(true);
            employeeProjectRepository.save(employeeProject);
        });


        String password = CommonUtil.tempPassword();
        User user = User.builder()
                .fullName(employee.getFullName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(password))
                .role(RoleType.ROLE_USER)
                .build();
        userRepository.save(user);

        //user field
        //?

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);

    }

    private CommonResponse setValueEmployee(List<Employee> employeeList) {
        List<EmployeeResponse> employees = new ArrayList<>();

        employeeList.forEach(data -> {
            EmployeeResponse employee = new EmployeeResponse();
            employee.setEmployeeId(data.getEmployeeId());
            employee.setFullName(data.getFullName());
            employee.setEmail(data.getEmail());
            employee.setPhoneNumber(data.getPhoneNumber());
            employee.setJoinDate(data.getJoinDate());
            employee.setDepartmentName(Objects.isNull(data.getDepartment()) ? null: data.getDepartment().getDepartmentName());
            employee.setJobTitle(data.getJob().getJobTitle());
            employees.add(employee);
        });

        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, employees);
    }

    public CommonResponse listManagers() {
        List<Employee> employees = repository.findAllByIsManager(true);
        return setValueEmployee(employees);
    }

    public CommonResponse listEmployees() {
        List<Employee> employeeList = repository.findAllByIsActive(true);
        return setValueEmployee(employeeList);
    }

    public CommonResponse deleteEmployeeById(Long employeeId) {
        Employee employee = repository.findById(employeeId).orElseThrow(() -> new CommonException(
                CommonMessage.NOT_FOUND_CODE,
                CommonMessage.NOT_FOUND_MESSAGE,
                String.format("Employee ID %d %s", employeeId, CommonMessage.NOT_FOUND_MESSAGE),
                HttpStatus.NOT_FOUND
        ));
        employee.setIsActive(true);
        repository.save(employee);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }
    public CommonResponse assignJob(AssignJobRequest request) {
        Optional<Employee> dataEmployee = repository.findById(request.getEmployeeId());
        AtomicReference<String> info = new AtomicReference<>();
        dataEmployee.ifPresentOrElse(data -> {
            data.setJob(jobService.getJobById(request.getJobId()));
            repository.save(data);
            info.set(CommonMessage.SUCCESS_MESSAGE);
        }, () -> info.set(CommonMessage.NOT_FOUND_MESSAGE));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, info.get(), info.get());
    }

    public CommonResponse assignDepartment(AssignDepartmentRequest request) {
        Optional<Employee> dataEmployee = repository.findById(request.getEmployeeId());
        AtomicReference<String> info = new AtomicReference<>();
        dataEmployee.ifPresentOrElse(data -> {
            data.setDepartment(departmentService.getDepartmentById(request.getDepartmentId()));
            repository.save(data);
            info.set(CommonMessage.SUCCESS_MESSAGE);
        }, () -> info.set(CommonMessage.NOT_FOUND_MESSAGE));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, info.get(), info.get());
    }

    public CommonResponse assignProject(AssignProjectRequest request) {
        Optional<Employee> dataEmployee = repository.findById(request.getEmployeeId());
        AtomicReference<String> info = new AtomicReference<>();
        dataEmployee.ifPresentOrElse(data -> {
            Set<Project> projects = new HashSet<>();
            request.getProjectIdList().forEach(projectId -> projects.add(projectService.getProjectById(projectId)));
            repository.save(data);
            info.set(CommonMessage.SUCCESS_MESSAGE);
        }, () -> info.set(CommonMessage.NOT_FOUND_MESSAGE));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, info.get(), info.get());
    }

    private static Employee setEmployeeAtomicValue(String employeeId, Optional<Employee> dataEmployee) {
        AtomicReference<Employee> employee = new AtomicReference<>(new Employee());
        dataEmployee.ifPresentOrElse(data -> {
            employee.get().setEmployeeId(data.getEmployeeId());
            employee.get().setFullName(data.getFullName());
            employee.get().setEmail(data.getEmail());
            employee.get().setPhoneNumber(data.getPhoneNumber());
            employee.get().setJob(data.getJob());
            employee.get().setClient(data.getClient());
        }, () -> {
            throw new CommonException(
                    CommonMessage.NOT_FOUND_CODE,
                    CommonMessage.NOT_FOUND_MESSAGE,
                    String.format("Employee %s %s", employeeId, CommonMessage.NOT_FOUND_MESSAGE),
                    HttpStatus.NOT_FOUND);
        });
        return employee.get();
    }

    static Employee getEmployeeAsManager(Long managerId, EmployeeRepository repository) {
        Optional<Employee> dataEmployee = repository.findByEmployeeIdAndIsManager(managerId, true);
        return setEmployeeAtomicValue(String.valueOf(managerId), dataEmployee);
    }

    public Employee getEmployeeById(Long employeeId) {
        Optional<Employee> dataEmployee = repository.findById(employeeId);
        return setEmployeeAtomicValue(String.valueOf(employeeId), dataEmployee);
    }

    public Employee getEmployeeByEmail(String email) {
        Optional<Employee> dataEmployee = repository.findByEmailAndIsActive(email, true);
        return setEmployeeAtomicValue(email, dataEmployee);
    }

    public long getNumberOfEmployees() {
        return repository.count();
    }

}
