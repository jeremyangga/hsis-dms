package com.example.dmshris.service;

import com.example.dmshris.exception.CommonException;
import com.example.dmshris.model.Client;
import com.example.dmshris.model.Employee;
import com.example.dmshris.model.Project;
import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AssignManagerProject;
import com.example.dmshris.payload.request.ProjectRequest;
import com.example.dmshris.payload.response.ProjectResponse;
import com.example.dmshris.repository.ClientRepository;
import com.example.dmshris.repository.EmployeeRepository;
import com.example.dmshris.repository.ProjectRepository;
import com.example.dmshris.util.CommonMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Repository
@Slf4j
public class ProjectService {
    private final ProjectRepository repository;
    private final EmployeeRepository employeeRepository;
    private final ClientRepository clientRepository;

    public ProjectService(ProjectRepository repository, EmployeeRepository employeeRepository, ClientRepository clientRepository) {
        this.repository = repository;
        this.employeeRepository = employeeRepository;
        this.clientRepository = clientRepository;
    }

    public CommonResponse addDataProject(ProjectRequest request) {
        if(repository.existsByProjectName(request.getProjectName())) {
            throw new CommonException(
                    CommonMessage.ALREADY_EXIST_CODE,
                    CommonMessage.ALREADY_EXIST_MESSAGE,
                    String.format("Project Name %s ".concat(CommonMessage.ALREADY_EXIST_MESSAGE), request.getProjectName()),
                    HttpStatus.FORBIDDEN
            );
        }

        if(Objects.isNull(request.getClientId())) {
            throw new CommonException(
                    CommonMessage.NOT_FOUND_CODE,
                    CommonMessage.NOT_FOUND_MESSAGE,
                    String.format("Client Id %s ".concat(CommonMessage.NOT_FOUND_MESSAGE), request.getClientId()),
                    HttpStatus.FORBIDDEN
            );
        }

        Project project = new Project();
        project.setProjectName(request.getProjectName());
//        project.setManager(Objects.isNull(request.getManagerId()) ? null : this.getManagerById(request.getManagerId()));
        project.setClient(this.getClientById(request.getClientId()));
        project.setStatus(true);
        repository.save(project);
        return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, CommonMessage.SUCCESS_MESSAGE);
    }

    public CommonResponse listAllProjects() {
        List<Project> projectList = repository.findAll();
        List<ProjectResponse> projects = projectList.stream().map(data -> new ProjectResponse(
                data.getProjectId(),
                data.getProjectName(),
                Objects.isNull(data.getManager()) ? null : data.getManager().getFullName()
        )).toList();

        if(projects.size() > 1) {
            return new CommonResponse(CommonMessage.SUCCESS_CODE, CommonMessage.SUCCESS_MESSAGE, projects);
        } else {
            return new CommonResponse(CommonMessage.NO_DATA_CODE, CommonMessage.NO_DATA_MESSAGE, CommonMessage.NO_DATA_MESSAGE);
        }
    }

    public Project getProjectById(Long projectId) {
        return repository.findById(projectId).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for project your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE
        ));
    }

    private CommonResponse assignManager(AssignManagerProject request) {
        Optional<Project> dataProject = repository.findById(request.getProjectId());
        AtomicReference<String> info = new AtomicReference<>();
        dataProject.ifPresentOrElse(data -> employeeRepository.findById(request.getManagerId()).ifPresentOrElse(manager ->{
            data.setManager(manager);
            repository.save(data);
            info.set(CommonMessage.SUCCESS_MESSAGE);
        },() -> info.set(CommonMessage.NOT_FOUND_MESSAGE)), () -> info.set(CommonMessage.NOT_FOUND_MESSAGE));
        return new CommonResponse(CommonMessage.SUCCESS_CODE, info.get(), info.get());
    }
//    private Employee getManagerById(Long managerId){
//        return getEmployeeAsManager(managerId, employeeRepository);
//    }

    private Client getClientById(long clientId) {
        return clientRepository.findById(clientId).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for client your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE
        ));
    }

    public Project getFirstProject() {
        return repository.findTopByStatusOrderByProjectIdDesc(true).orElseThrow(() -> new CommonException(
                CommonMessage.NO_DATA_CODE,
                CommonMessage.NO_DATA_MESSAGE,
                String.format("%s for job your choose", CommonMessage.NO_DATA_MESSAGE),
                HttpStatus.NOT_ACCEPTABLE
        ));
    }

}
