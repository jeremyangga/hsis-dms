package com.example.dmshris.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "departments", uniqueConstraints = {
        @UniqueConstraint(name = "department_name_uq", columnNames = "department_name")
})
public class Department {
    @Id
    @SequenceGenerator(name = "department_id_seq",
            sequenceName = "department_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_id_seq")
    private Long departmentId;
    @Column(name = "department_name", length = 100, nullable = false)
    private String departmentName;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_id")
    @JsonIgnore
    private Employee manager;
}
