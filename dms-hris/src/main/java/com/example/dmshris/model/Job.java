package com.example.dmshris.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "jobs", uniqueConstraints = {
        @UniqueConstraint(name = "job_title_uq", columnNames = "job_title")
})
public class Job {
    @Id
    @SequenceGenerator(name = "job_id_seq",
            sequenceName = "job_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "job_id_seq")
    @Column(name = "job_id", nullable = false)
    private Long jobId;

    @Column(name = "job_title", length = 50, nullable = false)
    private String jobTitle;

    @Column(name = "job_description")
    private String jobDescription;

}
