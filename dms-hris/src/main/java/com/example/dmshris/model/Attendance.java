package com.example.dmshris.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "attendances")
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "attendance_id", nullable = false)
    private UUID attendanceId;

    private LocalDateTime clockIn;
    private LocalDateTime clockOut;
    private LocalDate attendanceDate;
    private LocalDate createdDate;

    private String createdBy;

    @Column(name = "workplace")
    private String workplace;

    @Column(name = "event")
    private String event;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "project_id", nullable = false)
    @JsonIgnore
    private Project project;
    private String dailyTask;
    private Long totalWork;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "employee_id", nullable = false)
    @JsonIgnore
    private Employee employee;
}
