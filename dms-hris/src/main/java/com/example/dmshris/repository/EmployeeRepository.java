package com.example.dmshris.repository;

import com.example.dmshris.model.Employee;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Hidden
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    List<Employee> findAllByIsManager(Boolean isManager);
    List<Employee> findAllByIsActive(Boolean isActive);
    Optional<Employee> findByEmployeeIdAndIsManager(Long departmentId, Boolean isManager);
    boolean existsByEmail(String email);
    boolean existsByPhoneNumber(String phoneNumber);
    Optional<Employee> findByEmailAndIsActive(String email, boolean isActive);
}
