package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AssignManagerProject implements Serializable {
    @JsonProperty(value = "project_id")
    private Long projectId;

    @JsonProperty(value = "manager_id")
    private Long managerId;
}
