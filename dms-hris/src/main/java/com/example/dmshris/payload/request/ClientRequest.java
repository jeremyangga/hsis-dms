package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientRequest {
    @JsonProperty(value = "client_name")
    @NotBlank
    private String clientName;

    @Email
    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "notes")
    private String notes;

    @JsonProperty(value = "location")
    private LocationRequest location;
}
