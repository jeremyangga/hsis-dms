package com.example.dmshris.payload;

import com.example.dmshris.model.Attendance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceLog {
    Page<Attendance> attendances;
}
