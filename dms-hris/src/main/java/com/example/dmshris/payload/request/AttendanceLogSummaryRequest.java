package com.example.dmshris.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceLogSummaryRequest implements Serializable {
    private PeriodRequest period = new PeriodRequest();
}
