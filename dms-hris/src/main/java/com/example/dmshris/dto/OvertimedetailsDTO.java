package com.example.dmshris.dto;

public class OvertimedetailsDTO {
    private String fullName;
    private String startDate;
    private String endDate;
    private String reason;
    private String attachment;
    private String date;
    private String projectName;
    private String clientName;
    private String assessorName;

    public OvertimedetailsDTO(String fullName, String startDate, String endDate, String reason, String attachment, String date, String projectName, String clientName, String assessorName) {
        this.fullName = fullName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reason = reason;
        this.attachment = attachment;
        this.date = date;
        this.projectName = projectName;
        this.clientName = clientName;
        this.assessorName = assessorName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAssessorName() {
        return assessorName;
    }

    public void setAssessorName(String assessorName) {
        this.assessorName = assessorName;
    }
}
