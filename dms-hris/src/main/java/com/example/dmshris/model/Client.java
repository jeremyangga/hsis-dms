package com.example.dmshris.model;

import com.example.dmshris.util.TimesheetClientCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
@Table(name = "clients", uniqueConstraints = {
        @UniqueConstraint(name = "client_name_uq", columnNames = "client_name"),
        @UniqueConstraint(name = "client_email_uq", columnNames = "email")
})
public class Client {
    @Id
    @SequenceGenerator(name = "client_id_seq",
            sequenceName = "client_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id_seq")
    @Column(name = "client_id", nullable = false)
    private Long clientId;
    @Column(name = "client_name", nullable = false, length = 150)
    private String clientName;
    @Column(name = "email", length = 100)
    private String email;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    @JsonIgnore
    private Location location;

    private String notes;

    @Column(name = "timesheet_client_code", nullable = false, length = 150)
    @Enumerated(EnumType.STRING)
    private TimesheetClientCode timesheetClientCode;
}
