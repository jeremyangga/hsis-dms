package com.example.dmshris.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DepartmentRequest implements Serializable {
    @NotBlank
    @JsonProperty(value = "department_name")
    private String departmentName;

    @JsonProperty(value = "manager_id")
    private Long managerId;
}
