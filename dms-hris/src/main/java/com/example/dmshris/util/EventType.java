package com.example.dmshris.util;

public enum EventType {
    ANNUAL_LEAVE, SICK_LEAVE, MATERNITY_LEAVE, OTHER, PUBLIC_HOLIDAY, ATTEND
}
