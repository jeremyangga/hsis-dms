package com.example.dmshris.payload.response;

import com.example.dmshris.model.Attendance;
import com.example.dmshris.model.Employee;
import com.example.dmshris.model.Project;
import com.example.dmshris.util.EventType;
import com.example.dmshris.util.Workplace;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

public interface AttendanceLogInterface {
    Attendance getAttendance();
    UUID getAttendanceId();
    LocalDateTime getClockIn();
    LocalDateTime getClockOut();
    Workplace getWorkplace();
    EventType getEvent();
    Project getProject();
    String getDailyTask();
    Long getTotalWork();
    Employee getEmployee();
    LocalDate getAttendanceDate();
}
