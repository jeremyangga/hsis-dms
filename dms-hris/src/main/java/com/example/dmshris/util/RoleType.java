package com.example.dmshris.util;

public enum RoleType {
    ROLE_USER, ROLE_ADMIN, ROLE_HR
}
