package com.example.dmshris.controller;

import com.example.dmshris.payload.CommonResponse;
import com.example.dmshris.payload.request.AssignDepartmentRequest;
import com.example.dmshris.payload.request.AssignJobRequest;
import com.example.dmshris.payload.request.AssignProjectRequest;
import com.example.dmshris.payload.request.EmployeeRequest;
import com.example.dmshris.service.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("employees")
@Slf4j
public class EmployeeController {
    private final EmployeeService service;
    private final ObjectMapper mapper = new ObjectMapper();
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<CommonResponse> addDataEmployee(@Valid @RequestBody EmployeeRequest request) throws JsonProcessingException, URISyntaxException {
        log.info(String.format("Entering method addDataEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.addDataEmployee(request);
        log.info(String.format("Leaving method addDataEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<CommonResponse> getAllDataEmployees() throws JsonProcessingException {
        log.info(String.format("Entering method getAllDataEmployees on class %s", EmployeeController.class.getName()));
        CommonResponse response = service.listEmployees();
        log.info(String.format("Entering method getAllDataEmployees on class %s with response %s", EmployeeController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = {"/{employee-id}"})
    public ResponseEntity<CommonResponse> deleteEmployee(@PathVariable(name = "employee-id") Long employeeId) {
        log.info(String.format("Entering method deleteEmployee on class %s with employee-id %d", EmployeeController.class.getName(), employeeId));
        CommonResponse response = service.deleteEmployeeById(employeeId);
        log.info(String.format("Entering method deleteEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/managers")
    public ResponseEntity<CommonResponse> getAllDataManagers() throws JsonProcessingException {
        log.info(String.format("Entering method getAllDataManagers on class %s", EmployeeController.class.getName()));
        CommonResponse response = service.listManagers();
        log.info(String.format("Entering method getAllDataManagers on class %s with response %s", EmployeeController.class.getName(), mapper.writeValueAsString(response)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping("/assign-job")
    public ResponseEntity<CommonResponse> assignJobToEmployee(@Valid @RequestBody AssignJobRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method assignJobToEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignJob(request);
        log.info(String.format("Leaving method assignJobToEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/assign-department")
    public ResponseEntity<CommonResponse> assignDepartmentToEmployee(@Valid @RequestBody AssignDepartmentRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method assignDepartmentToEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignDepartment(request);
        log.info(String.format("Leaving method assignDepartmentToEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/assign-project")
    public ResponseEntity<CommonResponse> assignProjectToEmployee(@Valid @RequestBody AssignProjectRequest request) throws JsonProcessingException {
        log.info(String.format("Entering method assignProjectToEmployee on class %s with payload %s", EmployeeController.class.getName(), mapper.writeValueAsString(request)));
        CommonResponse response = service.assignProject(request);
        log.info(String.format("Leaving method assignProjectToEmployee on class %s", EmployeeController.class.getName()));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }


}
