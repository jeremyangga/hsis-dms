package com.example.dmshris.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "countries")
public class Country {
    @Id
    @SequenceGenerator(name = "country_id_seq",
            sequenceName = "country_id_seq",
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "country_id_seq")
    private Long countryId;
    @Column(name = "country_name", length = 60, nullable = false)
    private String countryName;
    @Column(name = "numeric_code", length = 3)
    private String numericCode;
    @Column(name = "alpha_2_code", length = 2)
    private String alpha2Code;

    @Column(name = "alpha_3_code", length = 3)
    private String alpha3Code;

}
